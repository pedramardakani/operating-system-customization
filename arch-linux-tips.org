* Package management:
** To see the latest installed packages:
   ls -ltr /var/cache/pacman/pkg/*

* Sound
** Commands to check:
   - ls -la /dev/snd/                  # To see all sound devices.
   - aplay -l                          # To list all hardware devices.
   - lsmod | grep '^snd' | column -t   # See all sound-related kernel modules.
   - speaker-test -c 2                 # Play noise from speakrs.
