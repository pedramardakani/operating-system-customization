* Basic information
  https://wiki.pine64.org/wiki/PinePhone_Pro
* Prepare microSD
** Copying the image
   1. You can either update the BIOS (firmware) or operating system.

      Tow-boot (firmware/BIOS):
      https://tow-boot.org/devices/pine64-pinephonePro.html

      Manjaro (OS: Phosh):
      https://github.com/manjaro-pinephone/phosh/releases

      ArchLinuxARM (OS: recommended Phosh)
      https://github.com/dreemurrs-embedded/Pine64-Arch/releases

   2. On your computer, run 'lsblk' to find the 'XXX' in '/dev/XXX' of
      your microSD card.

   3. Run the following commands to make sure it has no partition
      already (don't worry if the partition is already mounted, just
      delete them).

          sudo parted /dev/XXX
	  --> print
	  --> rm 2     # only if there is a 2nd partition.
	  --> print
	  --> rm 1     # only if a first partition remains.
	  --> ...
	  --> print    # There should be no partitions
	  --> mktable msdos
	  --> print    # Confirm the partition table

   4. Now you can run the following 'dd' command to copy the image
      into the microSD (don't forget to replace 'XXX').

          sudo dd if=archlinux-pinephone-pro-phosh-20220222.img \
                  of=/dev/XXX bs=1M oflag=direct,sync status=progress

   5. After it is finished, go into Parted again and see if the
      partition label can be printed? If so, then everything is fine,
      if not, Parted will say that it can fix it. Let Parted Fix it:

          sudo parted /dev/XXX
	  --> print

   6. You can now safely insert the microSD into the PinePhone to
      start the OS there.

* In Pinephone
** First setup
*** Disabling suspense and screen blank mode

    Make sure the following settings are done:

    1. In "settings" --> "power":
       1. Turn off "Dim Screen"
       2. Turn off "Screen Blank"
       3. Turn on "Show battery percentage"
       4. Turn off "Automatic Suspend" --> "On Battery Power"

*** Unknown trust error (with Pacman)

    If you get an unknown trust error from the update command, run
    these commands. Afterwards, run the update command above.

        sudo rm -r /etc/pacman.d/gnupg
        sudo pacman-key --init
        sudo pacman-key --populate danctnix archlinuxarm    # See below
        sudo pacman-key --refresh-keys
        sudo pacman -Sy gnupg danctnix-keyring archlinuxarm-keyring
        sudo pacman -Sc

    To get the list of keys to "populate", have a look here:

        ls /usr/share/pacman/keyrings

*** Fix the resolution
    Settings -> Display -> Scale --> 150%

*** Set hostname
    hostnamectl set-hostname name

*** Change the default Phosh user
    First define a new user

        sudo su
        useradd --create-home -s /bin/bash USERNAME
        passwd USERNAME

    Set the new user as the default user

        systemctl edit phosh

*** Set the timezone
    In "Settings", go to "Date and Time"
*** Software to install
    pacman -S make emacs gcc ispell nautilus wget
