This folder contains desktop files for special files and the icons you
can use for them.

Put the .desktop files in this folder:

cp *.desktop ~/.local/share/applications/

To make it default, right click on a file of that type that you want
and choose this application as default.
